package com.example.listviewdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    String data[]={"Varun","Truefalse","Ayushi","Sakshi","Chetna","Sukhpender","Navdeep","Neha","Shabnam","Reena","Vishwjeet","Gamni","Sarabjeet"};

    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView=findViewById(R.id.listview);
        ApnaAdapter apnaAdapter=new ApnaAdapter(data);
        listView.setAdapter(apnaAdapter);
    }
}
