package com.example.listviewdemo;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ApnaAdapter extends BaseAdapter {

    String name[];
    public ApnaAdapter(String names[])
    {
        name=names;
    }
    @Override
    public int getCount() {
        return name.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        View view;
        if(convertView==null)
        {
         viewHolder=new ViewHolder();
             convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.listdata, parent, false);
            viewHolder.textView = convertView.findViewById(R.id.text1);
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder=(ViewHolder) convertView.getTag();
        }
        viewHolder.textView.setText(name[position]);
        return convertView;
    }

    static class ViewHolder
    {
        TextView textView;
    }

}
